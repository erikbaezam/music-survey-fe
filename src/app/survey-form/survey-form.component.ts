import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { HttpClientService } from '../service/http-client.service';

@Component({
  selector: 'surveyForm',
  templateUrl: './survey-form.component.html',
  styleUrls: ['./survey-form.component.css'],
})
export class SurveyFormComponent implements OnInit {
  constructor(private httpClient: HttpClientService) {}

  form = new FormGroup({
    email: new FormControl(),
    generos: new FormControl(),
  });

  feedback: string = '';
  showTable = false;

  surveys: any = [];

  ngOnInit(): void {}

  async submit() {
    console.log(this.form.value);
    const response: any = await this.httpClient.saveSurvey(this.form.value);

    if (response && response.response === 'Succes') {
      this.feedback = 'Exito al guardar!';
      this.showSurveys();
    } else {
      this.feedback = 'Error al guardar!';
    }
  }

  async showSurveys() {
    this.surveys = await this.httpClient.getAll();
    this.showTable = true;
  }
}
