import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class HttpClientService {
  constructor(private httpClient: HttpClient) {}

  getAll() {
    return this.httpClient.get('http://localhost:8080/getAllResults').toPromise();
  }

  saveSurvey(form: any) {
    return this.httpClient.post('http://localhost:8080/saveSurvey', {
      email: form.email,
      genero: form.generos,
    }).toPromise();
  }
}
